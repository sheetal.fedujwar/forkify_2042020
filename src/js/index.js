import Search from './models/Search';
import Recipe from './models/Recipe';
import List from './models/List';
import Likes from './models/Likes';
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';
import * as listView from './views/listView';
import * as likesView from './views/likesView';
import { elements, renderLoader, clearLoader } from './views/base';


/** Global state of the app
 * - Search object
 * - Current recipe object
 * - Shopping list object
 * - Liked recipes
 */

 //Global state of the application
const globalState = {};

//Asynchronous function for the Search Object
async function getSearchResult()
{
    const searchValue = searchView.getInput();

    if(searchValue)
    {
        try
        {
            globalState.search = new Search(searchValue);
            searchView.clearInput();
            searchView.clearResults();
            renderLoader(elements.searchRes);
            await globalState.search.searchRecipe(); 
            clearLoader();
            searchView.renderResults(globalState.search.result);
        }
        catch(error)
        {
            Console.log(error);
            alert("Error occured while fetching Search data!!");
        }
    }
}

elements.searchForm.addEventListener("submit", e => {
    getSearchResult(); 
    e.preventDefault();
  });


//Asynchronous function for Recipe object

async function recipeContent()
{
    const recipeId = (window.location.href).substring(23);

    if (recipeId)
    {
        try
        {
            recipeView.clearRecipe();
            renderLoader(elements.recipe);        
        
            try
            {
                globalState.recipe = new Recipe(recipeId);

                await globalState.recipe.getRecipeDetails();
                globalState.recipe.calculatePrepTime();

                clearLoader();
                recipeView.renderRecipe(globalState.recipe);
            } 
            catch (error) 
            {
                console.log(error);
                alert("Error occurred while processing Recipe!!");
        }

        }
        catch(error)
        {
            Console.log(error);
            alert("Error occured while fetching Recipe data!!");
        }
    
        
      }
}

["hashchange", "load"].forEach(event => window.addEventListener(event, recipeContent));

const addItemToShoppingList = () => {

    if (!globalState.list) 
    globalState.list = new List();
    
    const item = globalState.list.addItem(globalState.recipe);
    listView.renderItem(item);

};

elements.recipe.addEventListener("click", e => {
      
    if (e.target.matches(".btn-decrease"))
    {
        globalState.recipe.updateServingsCount("decrease");
    }
    else if(e.target.matches(".btn-increase"))
    {
        globalState.recipe.updateServingsCount("increase");
    }

    if (e.target.matches(".recipe__btn--add")) {
        addItemToShoppingList();
    }
});


elements.shopping.addEventListener("click", e => {

    const id = e.target.closest(".shopping__item").dataset.itemid;

    if (e.target.matches(".shopping__delete")) {
      listView.deleteItem(id);
    }
});



