import axios from 'axios';

export default class Recipe 
{
    constructor(id)
    {
        this.id = id;
    }

    async getRecipeDetails()
    {
        try
        {                                                          
                const result = await axios.get('http://localhost:3000/recipeDetails', { params : {recipe_id: this.id }});
                const recipe = result.data;
                
                this.title = recipe[0].title;
                this.author = recipe[0].author;
                this.publisher = recipe[0].publisher;
                this.img =  recipe[0].image_url;
                this.servings =  recipe[0].servings;
                this.url =  recipe[0].source_url;
                this.ingredients =  recipe[0].ingredients;
        }
        catch (error)
        {
          console.log(error);
          alert("Something went wrong :C");
        }
      }

      calculatePrepTime()
       {
        const ingCount = this.ingredients.length;
        const set = Math.ceil(ingCount / 2);
        this.time = set * 10;
      }

      updateServingsCount(servingType) 
      {
          const newServingCount = servingType == "decrease" ? this.servings - 1 : this.servings + 1;
          this.servings = newServingCount;
          
          //updateIngredientQuantity(this.servings);
      }

      updateIngredientQuantity(servingCount)
      {
        //let lstIngredients = this.ingredients;
        //lstIngredients.for(let i = 0; i < lstIngredients.length; i++) {
         // const item = lstIngredients[i];
          //seperate number from ingredient
          //multiply with serving number
          //substitute updated measures
          
        
      }
}