
export default class List {

    constructor() {
        this.items = [];
      }
      addItem(recipe) {
        const item = {
          title : recipe[0].title,
          id : recipe[0].id,
        };
        this.items.push(item);
        return item;
      }

}