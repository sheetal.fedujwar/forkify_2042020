import axios from 'axios';

export default class Search {

   constructor(searchValue) 
   {
      this.searchValue = searchValue;
   }

   async searchRecipe() {
        try
        {
                const lstRecipe = await axios.get('http://localhost:3000/recipeDetails', { params : {name: this.searchValue }});
                this.result = lstRecipe.data;
        }
        catch(error)
        {
            Console.log(error);
            alert("Error occured while fetching Search data!!");
        }
    }
    
}
